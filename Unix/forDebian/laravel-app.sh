#!/bin/bash

#Updating sistem
updateRepositoriesSystem(){
	echo 'Updating repositories system..'
	sudo apt-get update
}

#Installing apache2 server
installApache2(){
	echo 'Installing Apache2'
	sudo apt-get install  -qq apache2
	sudo a2enmod rewrite
}

#Install git
installGit(){
	echo 'Installing git...'
	sudo apt-get install  -qq git
}

#Restart Apache2
restartApache2(){
	echo 'Restarting apache'
	sudo service apache2 restart
}


updatePPAPHP(){
	echo 'Updating PPA PHP...'
	sudo add-apt-repository -y ppa:ondrej/php
	updateRepositoriesSystem
}

#User choose php version that will be installed
installPHPVersion(){
	echo 'Installing PHP...'
	
	case $PHP_VERSION in
	'5.6')
	  	echo "Installing PHP5.6"
	  	updatePPAPHP
		sudo apt-get install  -qq -qq php5.6
		sudo apt-get install  -qq php5.6-dev
		sudo apt-get install  -qq php5.6-mysql
		sudo apt-get install  -qq php5.6-xml
		sudo apt-get install  -qq php5.6-mbstring
		sudo apt-get install  -qq php5.6-mcrypt
		sudo apt-get install  -qq php5.6-xdebug
		sudo apt-get install  -qq libapache2-mod-php5.6
		sudo apt-get install  -qq phpunit 
		sudo apt-get install  -qq php5.6-curl
		sudo ln -sfn /usr/bin/php5.6 /etc/alternatives/php
	  ;;
	'7.0')
	  	echo "Installing PHP7.0"
	  	updatePPAPHP
		sudo apt-get install  -qq php7.0
		sudo apt-get install  -qq php7.0-dev
		sudo apt-get install  -qq php7.0-mysql
		sudo apt-get install  -qq php7.0-xml
		sudo apt-get install  -qq php7.0-mbstring
		sudo apt-get install  -qq php7.0-xdebug
		sudo apt-get install  -qq libapache2-mod-php7.0
		sudo apt-get install  -qq phpunit 
		sudo apt-get install  -qq php7.0-curl
		sudo ln -sfn /usr/bin/php7.0 /etc/alternatives/php
	  ;;
	'7.1')
	  	echo "Installing PHP7.1"
	  	updatePPAPHP
		sudo apt-get install  -qq php7.1
		sudo apt-get install  -qq php7.1-dev
		sudo apt-get install  -qq php7.1-mysql
		sudo apt-get install  -qq php7.1-xml
		sudo apt-get install  -qq php7.1-mbstring
		sudo apt-get install  -qq php7.1-xdebug
		sudo apt-get install  -qq libapache2-mod-php7.1
		sudo apt-get install  -qq phpunit 
		sudo apt-get install  -qq php7.1-curl
		sudo ln -sfn /usr/bin/php7.1 /etc/alternatives/php
	  ;;
	*)
	  echo "Invalid Option"
	  installPHPVersion
	esac
}

#Install server Msql DB
installMysqlDB(){
	echo 'Installing MySQL...'
	echo "mysql-server-5.7 mysql-server/root_password password $MYSQL_ROOT_PASSWORD" | sudo debconf-set-selections
	echo "mysql-server-5.7 mysql-server/root_password_again password $MYSQL_ROOT_PASSWORD" | sudo debconf-set-selections
	sudo apt-get  -qq install mysql-server-5.7
}

#install OpenSSL Libraries Ubuntu 16
installOpenSSLLibraries(){
	sudo apt-get install  -qq autoconf g++ make openssl libssl-dev libcurl4-openssl-dev
	sudo apt-get install  -qq libcurl4-openssl-dev pkg-config
	sudo apt-get install  -qq libsasl2-dev
	sudo apt-get install  -qq libpcre3-dev
}


#Set vhost to the new project
settingProjectVHost(){
	echo 'Backup default file host'
	sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/$PROJECT_NAME.conf
	ERROR_FOLDER=$(echo $PROJECT_NAME | sed -e 's/\W/-/g')
	sudo mkdir /var/log/apache2/$ERROR_FOLDER/
	sudo echo -e  "<VirtualHost *:80>
        ServerName $PROJECT_URL
        ServerAlias $PROJECT_URL
        ServerAdmin webmaster@localhost
        DocumentRoot $PROJECT_PATH/public/
        <Directory $PROJECT_PATH/public/>
             Options Indexes FollowSymLinks
             AllowOverride All
             Require all granted
        </Directory>
        ErrorLog \${APACHE_LOG_DIR}/$ERROR_FOLDER/error.log
        CustomLog \${APACHE_LOG_DIR}/$ERROR_FOLDER/access.log combined
</VirtualHost>" | sudo tee /etc/apache2/sites-available/$PROJECT_NAME.conf
	
	sudo a2ensite $PROJECT_NAME.conf
	
	restartApache2
}

#Composer
installGlobalComposer(){
	echo 'Install unzip before install composer'
	sudo apt-get install  -qq unzip
	echo 'Installing global composer'
	php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    php composer-setup.php
    php -r "unlink('composer-setup.php');"
	sudo mv composer.phar /usr/local/bin/composer
}


#Create or Install project
installLaravelProject(){
	echo 'Installing project...'
	sudo chown -R $USER:www-data $PROJECTS_FOLDER
	cd $PROJECTS_FOLDER
	
	case "$PROJECT_GENERATE_TYPE_OPTION" in
		 y|Y ) 
			git clone $PROJECT_GIT_REPOSITORY
			PROJECT_NAME=$(ls -t $PROJECTS_FOLDER | head -1)
			;;
		 n|N )
			sudo composer create-project --prefer-dist laravel/laravel $PROJECT_NAME
			;;
	esac
	

	cd $PROJECT_PATH
	
	echo 'Running composer install. Wait...'
	composer install
	
	sudo chown -R $USER:www-data $PROJECT_PATH
	
	if [ ! -f $PROJECT_PATH/.env.example ]; then
    	echo "File .env.example not found!"
    else
    	echo 'Coping .env.example'
		cp .env.example .env
	
		echo 'Generating .env key'
		php artisan key:generate
	fi
	
	sudo chmod -R 0775 $PROJECT_PATH/storage/
	
}



#Set database config to Laravel .env
settingLaravelEnv(){
	echo 'Setting laravel .env...'
	cd $PROJECT_PATH/
	if [ ! -f .env ]; then
		echo "File .env not found!"
	else
		sed -i -- "s/DB_DATABASE=.*/DB_DATABASE=$PROJECT_NAME/g" .env
		sed -i -- 's/DB_USERNAME=.*/DB_USERNAME=root/g' .env
		sed -i -- "s/DB_PASSWORD=.*/DB_PASSWORD=$MYSQL_ROOT_PASSWORD/g" .env
		sed -i -- "s/APP_URL=.*/APP_URL=$PROJECT_URL/g" .env
	fi	
}

#Check if user is runnig in sudo mode
checkForRoot() {
    if [ ! $( id -u ) -eq 0 ]; then
        echo "ERROR: $0 Must be run as root, Script terminating" ;exit 7
    fi
}

#Create project database
createDatabase(){
	echo 'Creating database...'
	mysql -u "root" -p$MYSQL_ROOT_PASSWORD -Bse "CREATE DATABASE $PROJECT_NAME;"
}


#If project already exists the script request a database dump
runDumpDatabase(){
	echo 'Restoring database...'
	case "$PROJECT_GENERATE_TYPE_OPTION" in 
	  y|Y ) 
	  	read -p "Has DATABASE DUMP(D) or run the MIGRATIONS(M)? (D/M/n):" DATABASE_DUMP_OPTION
	  	case "$PROJECT_GENERATE_TYPE_OPTION" in
	  		d|D )
	  			read -p "Insert database dump SQL file path:" DUMP_PATH
	  			mysql -h "localhost" -u "root" -p$MYSQL_ROOT_PASSWORD $PROJECT_NAME < $DUMP_PATH
	  			;;
	  		m|M )
	  			cd $PROJECT_PATH/
	  			php artisan migrate
	  			;;
	  		n|N )
	  			echo 'Finishing script...'
	  			;;
	  		* )
	  			echo 'Invalid Option!'
	  			runDumpDatabase
	  	esac
	  	;;
	esac
}

setHosts(){
	echo "Updating hosts..."
	sudo echo "127.0.0.1	$PROJECT_URL" >> /etc/hosts
}

#Get repository name or project name
getProject(){
	read -p "Project exists? (y/n)?" PROJECT_GENERATE_TYPE_OPTION
	case "$PROJECT_GENERATE_TYPE_OPTION" in 
	  y|Y ) 
	  	read -p "Git remote repository: " PROJECT_GIT_REPOSITORY;;
	  n|N ) 
	  	read -p "What's the project name?: " PROJECT_NAME;;
	  * ) 
	  	echo "Invalid option"
	  	getProject
	  	;;
	esac
}

getURLProject(){
	read -p "Project URL: " PROJECT_URL
}

getPHPVersion(){
	read -p "Choose PHP version: 5.6|7.0|7.1 (Default: 5.6): " PHP_VERSION
	case $PHP_VERSION in
	'5.6'| '7.0' | '7.1' )
		echo $PHP_VERSION
		;;
	'' )
		PHP_VERSION='5.6'
		echo $PHP_VERSION
		;;	
	* )
		echo 'Invalid Option'
		getPHPVersion
	esac	
}

getMySQLPassword(){
	read -p "Please enter MySQL root user password: " MYSQL_ROOT_PASSWORD
}



PROJECTS_FOLDER=/var/www/

checkForRoot

getProject
getURLProject
getPHPVersion
getMySQLPassword

PROJECT_PATH=/var/www/$PROJECT_NAME

installApache2
installMysqlDB
installPHPVersion
installOpenSSLLibraries
installGit
installGlobalComposer
installLaravelProject
settingProjectVHost
settingLaravelEnv
setHosts
restartApache2
runDumpDatabase

echo 'Installation is complete.'
echo 'Your app is ready. Access it in http://'$PROJECT_URL







