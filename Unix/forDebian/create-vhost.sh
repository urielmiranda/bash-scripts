#!/bin/sh

func_check_for_root() {
        if [ ! $( id -u ) -eq 0 ]; then
            echo "ERROR: $0 Must be run as root, Script terminating" ;exit 7
        fi
    }
    func_check_for_root

echo "Please enter vhost url: "
read VHOST_URL

echo "Please enter with project public folder path: "
read VHOST_PUBLIC_FOLDER


"Creating log file"
sudo mkdir /var/log/apache2/$VHOST_URL/
sudo chown -R www-data:www-data /var/log/apache2/$VHOST_URL/


echo "Writing in new conf file"
sudo echo -e  "<VirtualHost *:80>
        ServerName $VHOST_URL
        ServerAlias $VHOST_URL
        ServerAdmin webmaster@localhost
        DocumentRoot $VHOST_PUBLIC_FOLDER
        <Directory $VHOST_PUBLIC_FOLDER>
             Options Indexes FollowSymLinks
             AllowOverride All
             Require all granted
        </Directory>
        ErrorLog \${APACHE_LOG_DIR}/$VHOST_URL/error.log
        CustomLog \${APACHE_LOG_DIR}/$VHOST_URL/access.log combined
</VirtualHost>" | sudo tee /etc/apache2/sites-available/$VHOST_URL.conf

echo "Enabling new site"
sudo a2ensite $VHOST_URL.conf

echo "Updating hosts"
sudo echo "127.0.0.1	$VHOST_URL" >> /etc/hosts

echo "Restart server"
sudo service apache2 restart

