# Usefull Bash #

### About the project ###

* This project aims to put together some bash scripts that are used daily in the life of a programmer. 
* Please if you have any usefull script, drop him here.

### Contribution guidelines ###

* When you add any bash here, please edit this readme.md with the archive name and the instructions to run it.

## Scripts Unix/Debian ##

### Create Virtual Host Apache2 - For Debian Distributions ###
* Archive: create-vhost.sh
* Run: sudo create-vhost.sh
* During the script will be requested the path to site index and the vhost name
* After installation, just access your vhost through the browser.

### Install a Complete Environment for new or existing Laravel 5 App ###
* Archive: laravel-app.sh
* Run: sudo laravel-app.sh
* Install: Apache2, Mysql, PHP(5.6|7.0|7.1), Git, Composer, Laravel 5.
* Set a virtual host for the application

### Any doubt? ###

* Uriel Miranda
* Please contact miranda.uriel@gmail.com